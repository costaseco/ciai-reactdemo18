
export interface IGenericAction { type: string }

export interface ISelectCompanyAction { type: string, selectedCompany: ICompany }

export interface ICompany {
    name: string;
    address: string;
    city: string;
    country: string;
    logo: string
}

export interface IState {
    companies: ICompany[],
    selectedCompany: ICompany | null,
}

const initCompanies = [
    {
        "address":"962-1710 Arcu. Avenue",
        "city":"Rockford",
        "country":"Mauritania",
        "logo":"./img/logo.png",
        "name":"Auctor Velit Ltd"
    },
    {
        "address":"P.O. Box 456, 9714 Vitae Street",
        "city":"Medemblik",
        "country":"Serbia",
        "logo":"./img/logo.png",
        "name":"Hendrerit Id Ante Associates"
    },
    {
        "address":"Ap #174-3057 Vehicula Rd.",
        "city":"Algeciras",
        "country":"Micronesia",
        "logo":"./img/logo.png",
        "name":"Phasellus Elit Limited"
    },
    ];

export const mainReducer =
(
    state: IState = { companies: initCompanies, selectedCompany: null },
    action : IGenericAction | ISelectCompanyAction
) => {
    switch (action.type) {
        case 'SELECT_COMPANY_ACTION':
            return {
                ...state,
                selectedCompany: (action as ISelectCompanyAction).selectedCompany
            };
        default:
            return state;
    }
};

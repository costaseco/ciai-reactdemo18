import * as React from 'react';
import CompanyDetails from './CompanyDetails';
import CompanyList from './CompanyList';

const App = () => (
    <ul>
        <CompanyList/>
        <CompanyDetails/>
    </ul>
);

export default App;
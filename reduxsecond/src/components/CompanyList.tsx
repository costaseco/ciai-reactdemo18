import * as React from "react";
import {connect} from "react-redux";
import {Dispatch} from "redux";
import {ICompany, ISelectCompanyAction, IState} from "../reducers/State";

interface ICompanyListProps {
    companies: ICompany[],
    selectCompany: (c:ICompany) => void
}

const CompanyListComponent = ({companies, selectCompany} : ICompanyListProps ) => (
    <ul>
        { companies.map(
            c => <li key={c.name} onClick={()=> selectCompany(c)}>{c.name}</li>
        )}
    </ul>
);

const mapStateToProps = (state:IState) => ({ companies: state.companies });

const mapDispatchToProps = (dispatch:Dispatch<ISelectCompanyAction>) => ({
    selectCompany: (selectedCompany:ICompany) => dispatch({
        selectedCompany,
        type: 'SELECT_COMPANY_ACTION'
    })
});

const CompanyList = connect(mapStateToProps,mapDispatchToProps)(CompanyListComponent);

export default CompanyList;
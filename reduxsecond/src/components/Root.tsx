import * as React from 'react';
import { Provider } from 'react-redux';
import {applyMiddleware, createStore} from "redux";
import { createLogger } from 'redux-logger';
import thunkMiddleware from 'redux-thunk';

import { mainReducer } from '../reducers/State';
import App from "./App";

const store = createStore(mainReducer, applyMiddleware(createLogger(), thunkMiddleware));

const Root = () => (
    <Provider store={store}>
        <App />
    </Provider>
);

export default Root;

import * as React from 'react';
import {connect} from "react-redux";
import {ICompany, IState} from "../reducers/State";

const CompanyDetailsComponent = ({company}: { company: ICompany | null }) => (
     (company !== null) ?
        <div>
            <img src={company.logo}/>
            <h2>{company.name}</h2>
            <p>{company.address}({company.city}, {company.country})</p>
        </div>
         :
        <p> Please select a company </p>
);

const mapStateToProps = (state: IState) => ({company: state.selectedCompany});

const CompanyDetails = connect(mapStateToProps)(CompanyDetailsComponent);

export default CompanyDetails;
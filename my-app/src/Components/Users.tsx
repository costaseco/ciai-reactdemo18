import * as React from "react";
import {FilteredList} from "../Common/FilteredList";

export interface User {
    name: string;
    age: number;
    email: string;
    telephone: string;
}

export interface IUsersList {
    list: User[];
    select: (x: number) => void;
    page?: number;
    pagesize?: number;
}

export class UsersList extends React.Component<IUsersList,{}> {
    constructor(props:IUsersList) {
        super(props);
    }

    public render() {
        return <FilteredList<User> {...this.props}
                                      title={"Users"}
                                      show={this.show}
                                      predicate={this.predicate}/>
    }

    private show = (c:User) => `${c.name} (${c.age})`;

    private predicate = (c:User,s:string) => (c.name+c.email).indexOf(s) !== -1;
}

export const Users = (props: { user: User }) => (
    <div>
        <h2>{props.user.name} ({props.user.age})</h2>
        <p>{props.user.email}</p>
        <p>{props.user.telephone}</p>
    </div>);


import axios from 'axios';
import * as React from 'react';
import {Fragment} from 'react';
import {Col, Grid, PageHeader, Row} from 'react-bootstrap';

import './App.css';
import Clock from './Common/Clock';

import {Company, CompanyDetails, CompanyList} from "./Components/Companies";
import {Users, UsersList} from "./Components/Users";


interface IApp {
    selectedCompany: number | null;
    selectedUser: number | null;
    companies: Company[];
}

class App extends React.Component<{}, IApp> {

    constructor(props: {}) {
        super(props);
        this.state = {selectedCompany: null, selectedUser: null, companies: []};
    }

    public componentWillMount() {
        axios
            .get('./companieswemployees.json')
            .then((json: any) => {
                this.setState({companies: json.data});
            });
    }

    public render() {
        return (
            <Fragment>
            <PageHeader>
                This is a sample application with React and Bootstrap (CIAI 2018)
                <Clock/>
            </PageHeader>
            <Grid>
                <Row>
                    <Col md={4}>
                    <CompanyList list={this.state.companies}
                                 select={this.selectCompany}
                                 pagesize={10}/>
                    </Col>
                    <Col md={4}>
                    {this.state.selectedCompany !== null &&
                    <CompanyDetails company={this.state.companies[this.state.selectedCompany]}/>}
                    </Col>
                    <Col md={4}>
                    {this.state.selectedCompany !== null &&
                    this.state.companies[this.state.selectedCompany].employees.length > 0 &&
                    <UsersList list={this.state.companies[this.state.selectedCompany].employees}
                               select={this.selectUser}/>}

                    {this.state.selectedCompany !== null &&
                    this.state.selectedUser !== null &&
                    <Users user={this.state.companies[this.state.selectedCompany].employees[this.state.selectedUser]}/>}
                    </Col>
                </Row>
            </Grid>
            </Fragment>
        );
    }

    private selectCompany = (x: number) => {
        this.setState({selectedCompany: x, selectedUser: null})
    };

    private selectUser = (x: number) => {
        this.setState({selectedUser: x})
    };

}

export default App;

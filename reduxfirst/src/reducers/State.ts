export interface IGenericAction { type: string };

export type State = number;

export const mainReducer =
    (
        state: State = 0,
        action : IGenericAction
    ) => {
        switch (action.type) {
            case 'INC_ACTION': return state+1;
            case 'DEC_ACTION': return state-1;
            default:
                return state;
        }
};
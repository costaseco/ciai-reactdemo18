import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { IGenericAction, State } from '../reducers/State';

const AppComponent =
    (
        props:
            {count:number,
            inc: () => void,
            dec: () => void }
    ) => (
    <div>
        {props.count}
        <button onClick={props.inc}>Inc</button>
        <button onClick={props.dec}>Dec</button>
    </div>
);

const mapStateToProps = (state: State) => ({count: state});

const mapDispatchToProps = (dispatch: Dispatch<IGenericAction>) => {
    return {
        inc: () => {
            dispatch({ type: 'INC_ACTION'});
        },

        dec: () => {
            dispatch({ type: 'DEC_ACTION'});
        },
    };
};

const App = connect(mapStateToProps, mapDispatchToProps)(AppComponent);

export default App;